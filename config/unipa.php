<?php

return [

    'username' => env('UNIPA_USERNAME'),

    'password' => env('UNIPA_PASSWORD'),

];
