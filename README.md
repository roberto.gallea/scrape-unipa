# Unipa scrape example

## Install dependencies

`composer install`

## Configure unipa credentials

- Copy env file 

`mv .env.example .env`

- Update required environment variables

```
...

UNIPA_USERNAME=<your-username>
UNIPA_PASSWORD=<your-password>

...
```
## Start chrome webdriver

`chromedriver --port=4444`

## Run demo

`php artisan app:scrape-unipa`
