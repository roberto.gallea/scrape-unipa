<?php

namespace App\Operations;

use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverSelect;

class Exam
{

    public function __construct()
    {
    }

    public function execute(\Facebook\WebDriver\Remote\RemoteWebDriver $driver)
    {
        $driver->get('https://immaweb.unipa.it/immaweb/private/docenti/esami/prenotazioniStudente.seam');
        sleep(5);

        $headerLabels = $driver->findElements(WebDriverBy::cssSelector('#prenotazioniStudenteForm table > thead > tr > th'));
        $labels = collect($headerLabels)->map(fn($i) => $i->getText());
        $rows = $driver->findElements(WebDriverBy::cssSelector('#prenotazioniStudenteForm table > tbody > tr'));

        return [
            'labels' => $labels,
            'rows' => collect($rows),
        ];
    }

    public function dump($exams)
    {
        $labels = $exams['labels'];
        $exams = $exams['rows'];

        $exams->each(function (RemoteWebElement $row, $i) use ($labels) {
            $cells = $row->findElements(WebDriverBy::cssSelector(':scope > td'));
            print("Prenotazione #" . $i . "\n");
            collect($cells)->each(function (RemoteWebElement $cell, $j) use ($labels) {
                print($labels[$j] . ": ". $cell->getText() . "\n");
            });
            print("\n\n");
        });
    }
}
