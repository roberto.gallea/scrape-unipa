<?php

namespace App\Operations;

use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;

class A3Data
{

    public function __construct()
    {
    }

    public function execute(\Facebook\WebDriver\Remote\RemoteWebDriver $driver)
    {
        $driver->get('https://a3.unipa.it/profile');
        sleep(5);
        $tab = $driver->findElement(WebDriverBy::cssSelector('#user'));
        $driver->executeScript("arguments[0].setAttribute('class','tab-pane active')", [$tab]);

        $labels = $driver->findElements(WebDriverBy::cssSelector('#user dl > dt'));
        $labels = collect($labels)->map(fn($i) => $i->getText());
        $labels->splice(3, 0, "Validità password");

        $values = $driver->findElements(WebDriverBy::cssSelector('#user dl > dd'));
        $values = collect($values)->map(fn($i) => $i->getText());
        return [
            'labels' => $labels,
            'values' => $values,
        ];
    }

    public function dump(array $a3Data)
    {
        $labels = $a3Data['labels'];
        $values = $a3Data['values'];

        print("Dati utente\n");
        $values->each(function (string $value, $i) use ($labels) {
            print($labels[$i] . ": " . $value . "\n");
            print("\n\n");
        });
    }
}
