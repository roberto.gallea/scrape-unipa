<?php

namespace App\Operations;

use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;

class A3Position
{

    public function __construct()
    {
    }

    public function execute(\Facebook\WebDriver\Remote\RemoteWebDriver $driver)
    {
        $driver->get('https://a3.unipa.it/profile');
        sleep(5);
        $tab = $driver->findElement(WebDriverBy::cssSelector('#positions'));
        $driver->executeScript("arguments[0].setAttribute('class','tab-pane active')", [$tab]);

        $headerLabels = $driver->findElements(WebDriverBy::cssSelector('#positions table > thead > tr > th'));
        $labels = collect($headerLabels)->map(fn($i) => $i->getText());
        $rows = $driver->findElements(WebDriverBy::cssSelector('#positions table > tbody > tr'));

        return [
            'labels' => $labels,
            'rows' => collect($rows),
        ];
    }

    public function dump(array $a3Positions)
    {
        $labels = $a3Positions['labels'];
        $a3Positions = $a3Positions['rows'];

        $a3Positions->each(function (RemoteWebElement $row, $i) use ($labels) {
            $cells = $row->findElements(WebDriverBy::cssSelector(':scope > td'));
            print("Posizione #" . $i . "\n");
            collect($cells)->each(function (RemoteWebElement $cell, $j) use ($labels) {
                print($labels[$j] . ": ". $cell->getText() . "\n");
            });
            print("\n\n");
        });
    }
}
