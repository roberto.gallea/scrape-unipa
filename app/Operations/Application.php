<?php

namespace App\Operations;

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverSelect;

class Application
{

    public function __construct()
    {
    }

    public function execute(RemoteWebDriver $driver)
    {
        $driver->get('https://immaweb.unipa.it/immaweb/private/pratiche/listPratiche.seam');
        sleep(5);
        $select = new WebDriverSelect(
            $driver->findElement(WebDriverBy::name('ricListPratiche:viewPraticheDecorate:viewPratiche'))
        );
        $selectedOptions = $select->getAllSelectedOptions();
        $select->selectByVisibleText('Tutte le Pratiche');
        $driver->findElement(WebDriverBy::name('ricListPratiche:viewPraticheDecorate:j_id69'))->click();
        sleep(2);

        $headerLabels = $driver->findElements(WebDriverBy::cssSelector('#ricListPratiche\\:listPratiche > thead > tr > th'));
        $labels = collect($headerLabels)->map(fn($i) => $i->getText());
        $rows = $driver->findElements(WebDriverBy::cssSelector('#ricListPratiche\\:listPratiche > tbody > tr'));

        return [
            'labels' => $labels,
            'rows' => collect($rows),
        ];
    }

    public function dump(array $applications)
    {
        $labels = $applications['labels'];
        $applications = $applications['rows'];

        $applications->each(function (RemoteWebElement $row, $i) use ($labels) {
            $cells = $row->findElements(WebDriverBy::cssSelector(':scope > td'));
            print("Pratica #" . $i . "\n");
            collect($cells)->each(function (RemoteWebElement $cell, $j) use ($labels) {
                print($labels[$j] . ": ". $cell->getText() . "\n");
            });
            print("\n\n");
        });

    }
}
