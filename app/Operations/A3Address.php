<?php

namespace App\Operations;

use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;

class A3Address
{

    public function __construct()
    {
    }

    public function execute(\Facebook\WebDriver\Remote\RemoteWebDriver $driver)
    {
        $driver->get('https://a3.unipa.it/profile');
        sleep(5);
        $tab = $driver->findElement(WebDriverBy::cssSelector('#addresses'));
        $driver->executeScript("arguments[0].setAttribute('class','tab-pane active')", [$tab]);

        $headerLabels = $driver->findElements(WebDriverBy::cssSelector('#addresses table > thead > tr > th'));
        $labels = collect($headerLabels)->map(fn($i) => $i->getText());
        $rows = $driver->findElements(WebDriverBy::cssSelector('#addresses table > tbody > tr'));

        return [
            'labels' => $labels,
            'rows' => collect($rows),
        ];
    }

    public function dump(array $a3Addresses)
    {
        $labels = $a3Addresses['labels'];
        $a3Addresses = $a3Addresses['rows'];

        $a3Addresses->each(function (RemoteWebElement $row, $i) use ($labels) {
            $cells = $row->findElements(WebDriverBy::cssSelector(':scope > td'));
            print("Indirizzo #" . $i . "\n");
            collect($cells)->each(function (RemoteWebElement $cell, $j) use ($labels) {
                print($labels[$j] . ": ". $cell->getText() . "\n");
            });
            print("\n\n");
        });
    }
}
