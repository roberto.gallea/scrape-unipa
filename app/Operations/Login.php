<?php

namespace App\Operations;

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;

class Login
{

    public function __construct(private string $username, private string $password)
    {
    }

    public function execute(RemoteWebDriver $driver)
    {
        $driver->get('https://a3.unipa.it');
        $driver->findElement(WebDriverBy::cssSelector("#profile-navigation > li > a"))->click();
        sleep(2);
        $driver->findElement(WebDriverBy::name('j_username'))->sendKeys($this->username);
        $driver->findElement(WebDriverBy::name('j_password'))->sendKeys($this->password);
        $driver->findElement(WebDriverBy::name('_eventId_proceed'))->click();
        sleep(2);
    }
}
