<?php

namespace App\Operations;

use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;

class DeletedSubscription
{

    public function __construct()
    {
    }

    public function execute(\Facebook\WebDriver\Remote\RemoteWebDriver $driver)
    {
        $driver->get('https://immaweb.unipa.it/immaweb/private/docenti/esami/iscrizioneLezioni.seam');
        sleep(5);


        $tab = $driver->findElement(WebDriverBy::cssSelector('#iscrizioniCancellateTab'));
        $driver->executeScript("arguments[0].setAttribute('class','tab-pane active')", [$tab]);

        $headerLabels = $driver->findElements(WebDriverBy::cssSelector('#j_id52\\:iscrizioniCancellateEffettuateTable > thead > tr > th'));
        $labels = collect($headerLabels)->map(fn($i) => $i->getText());
        $rows = $driver->findElements(WebDriverBy::cssSelector('#j_id52\\:iscrizioniCancellateEffettuateTable > tbody > tr'));

        return [
            'labels' => $labels,
            'rows' => collect($rows),
        ];
    }

    public function dump(array $subscriptions)
    {
        $labels = $subscriptions['labels'];
        $exams = $subscriptions['rows'];

        $exams->each(function (RemoteWebElement $row, $i) use ($labels) {
            $cells = $row->findElements(WebDriverBy::cssSelector(':scope > td'));
            print("Iscrizione #" . $i . "\n");
            collect($cells)->each(function (RemoteWebElement $cell, $j) use ($labels) {
                print($labels[$j] . ": ". $cell->getText() . "\n");
            });
            print("\n\n");
        });
    }
}
