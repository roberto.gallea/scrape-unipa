<?php

namespace App\Operations;

use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;

class StudyPlan
{

    public function __construct()
    {
    }

    public function execute(\Facebook\WebDriver\Remote\RemoteWebDriver $driver)
    {
        $driver->get('https://immaweb.unipa.it/immaweb/private/docenti/esami/prenotazioneEsami.seam');
        sleep(5);
        $headerLabels = $driver->findElements(WebDriverBy::cssSelector('.panel-theme-studenti thead > tr > th'));
        $labels = collect($headerLabels)->map(fn($headerLabel) => $headerLabel->getText());
        var_dump($labels);
        $rows = $driver->findElements(WebDriverBy::cssSelector('.pianostudi-detail'));

        $valori = $driver->findElements(WebDriverBy::cssSelector('#prenotazioneEsame\\:j_id89 td span.outputTextValue'));

        $mediaAritmetica = $valori[0]->getText();
        $mediaPonderata = $valori[1]->getText();
        $baseVotoAritmetica = $valori[2]->getText();
        $baseVotoPonderata = $valori[3]->getText();
        $voto = $valori[4]->getText();
        $statoCarriera = $valori[5]->getText();

        return [
            'labels' => $labels,
            'rows' => collect($rows),
            'media_aritmetica' => $mediaAritmetica,
            'media_ponderata' => $mediaPonderata,
            'base_voto_aritmetica' => $baseVotoAritmetica,
            'base_voto_ponderata' => $baseVotoPonderata,
            'voto' => $voto,
            'stato_carriera' => $statoCarriera,
        ];
    }

    public function dump(array $studyPlan)
    {
        $labels = $studyPlan['labels'];
        $pagamenti = $studyPlan['rows'];

        $pagamenti->each(function (RemoteWebElement $row, $i) use ($labels) {
            $cells = $row->findElements(WebDriverBy::tagName('td'));
            print("Materia #" . $i . "\n");
            collect($cells)->each(function (RemoteWebElement $cell, $j) use ($labels) {
                print($labels[$j] . ": ". $cell->getText() . "\n");
            });
            print("\n\n");
        });

        print("Media aritmetica: {$studyPlan['media_aritmetica']}\n");
        print("Media ponderata: {$studyPlan['media_ponderata']}\n");
        print("Base voto (media aritmetica): {$studyPlan['base_voto_aritmetica']}\n");
        print("Base voto (media ponderata): {$studyPlan['base_voto_ponderata']}\n");
        print("Voto laurea: {$studyPlan['voto']}\n");
        print("Stato carriera: {$studyPlan['stato_carriera']}\n");
        print("\n\n");

    }
}
