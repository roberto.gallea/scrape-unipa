<?php

namespace App\Console\Operations;

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverSelect;

class Payments
{

    public function __construct()
    {
    }

    /**
     * @param RemoteWebDriver $driver
     * @param string $year
     * @return array
     * @throws \Facebook\WebDriver\Exception\NoSuchElementException
     * @throws \Facebook\WebDriver\Exception\UnexpectedTagNameException
     */
    public function execute(RemoteWebDriver $driver, string $year)
    {
        $driver->get('https://immaweb.unipa.it/immaweb/private/bollettini/bollettiniList.seam');
        sleep(5);
        $select = new WebDriverSelect(
            $driver->findElement(WebDriverBy::name('j_id50:annoAccademicoDecorate:annoAccademico'))
        );
        $selectedOptions = $select->getAllSelectedOptions();
//        dump($selectedOptions[1]->getText());
        $select->selectByVisibleText($year);
        $driver->findElement(WebDriverBy::name('j_id50:annoAccademicoDecorate:btnSearchForAnnoAccademico'))->click();
        sleep(2);
        $headerLabels = $driver->findElements(WebDriverBy::cssSelector('#j_id50\\:listaMav > thead > tr > th'));
        $labels = collect($headerLabels)->map(fn($headerLabel) => $headerLabel->getText());
        $rows = $driver->findElements(WebDriverBy::cssSelector('#j_id50\\:listaMav > tbody > tr'));


        return [
            'labels' => $labels,
            'rows' => collect($rows)
        ];
    }

    public function dump(array $pagamenti)
    {
        $labels = $pagamenti['labels'];
        $pagamenti = $pagamenti['rows'];

        $pagamenti->each(function (RemoteWebElement $row, $i) use ($labels) {
            $cells = $row->findElements(WebDriverBy::tagName('td'));
            print("agamento #" . $i . "\n");
            collect($cells)->each(function (RemoteWebElement $cell, $j) use ($labels) {
                print($labels[$j] . ": ". $cell->getText() . "\n");
            });
            print("\n\n");
        });

    }
}
