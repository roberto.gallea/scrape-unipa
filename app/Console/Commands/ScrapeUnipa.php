<?php

namespace App\Console\Commands;

use App\Console\Operations\Payments;
use App\Operations\A3Address;
use App\Operations\A3Data;
use App\Operations\A3Position;
use App\Operations\A3Unipacard;
use App\Operations\Application;
use App\Operations\DeletedSubscription;
use App\Operations\Enrollment;
use App\Operations\Exam;
use App\Operations\Login;
use App\Operations\Message;
use App\Operations\StudyPlan;
use App\Operations\ActiveSubscription;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Console\Command;

class ScrapeUnipa extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:scrape-unipa';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $serverUrl = 'http://localhost:4444';
        $driver = RemoteWebDriver::create($serverUrl, DesiredCapabilities::chrome());

        (new Login(username: config('unipa.username'), password: config('unipa.password')))->execute($driver);

        $paymentOperation = new Payments;
        $payments = $paymentOperation->execute($driver, date('Y'));
        $paymentOperation->dump($payments);

        $studyPlanOperations = new StudyPlan();
        $studyPlan = $studyPlanOperations->execute($driver);
        $studyPlanOperations->dump($studyPlan);

        $applicationOperations = new Application();
        $applications = $applicationOperations->execute($driver);
        $applicationOperations->dump($applications);

        $enrollmentOperations = new Enrollment();
        $enrollments = $enrollmentOperations->execute($driver);
        $enrollmentOperations->dump($enrollments);

        $examOperations = new Exam();
        $exams = $examOperations->execute($driver);
        $examOperations->dump($exams);

        $subscriptionOperations = new ActiveSubscription();
        $subscriptions = $subscriptionOperations->execute($driver);
        $subscriptionOperations->dump($subscriptions);

        $subscriptionOperations = new DeletedSubscription();
        $subscriptions = $subscriptionOperations->execute($driver);
        $subscriptionOperations->dump($subscriptions);

        $messageOperations = new Message();
        $messages = $messageOperations->execute($driver);
        $messageOperations->dump($messages);

        $a3DataOperations = new A3Data();
        $a3Data = $a3DataOperations->execute($driver);
        $a3DataOperations->dump($a3Data);

        $a3PositionOperations = new A3Position();
        $a3Positions = $a3PositionOperations->execute($driver);
        $a3PositionOperations->dump($a3Positions);

        $a3AddressOperations = new A3Address();
        $a3Addresses = $a3AddressOperations->execute($driver);
        $a3AddressOperations->dump($a3Addresses);

        $a3UnipacardOperations = new A3Unipacard();
        $a3Unipacards = $a3UnipacardOperations->execute($driver);
        $a3UnipacardOperations->dump($a3Unipacards);


        $driver->close();


    }
}
